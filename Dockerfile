FROM elixir:latest

RUN apt update -y
RUN apt install gnuplot -y

RUN mkdir -p /data

# Create and set home directory
WORKDIR /opt/xml_reducer

# Configure required environment
ENV MIX_ENV prod

# Install hex (Elixir package manager)
RUN mix local.hex --force

# Install rebar (Erlang build tool)
RUN mix local.rebar --force

# Copy all dependencies files
COPY mix.* ./

# Install all production dependencies
RUN mix deps.get --only prod

# Compile all dependencies
RUN mix deps.compile

# Copy all application files
COPY . .

# Compile the entire project
RUN mix compile

RUN mix escript.build

# Run the application itself
#CMD ./scripts/run_local_parallel.sh
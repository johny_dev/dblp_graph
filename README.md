# dblp.org publication countr - XmlReducer

## Instructions

There are 3 ways to process the xml. Localy, Local and parallel, or remote web.

Before prepare your docker machine and run it

- Grub the xml from https://dblp.org/xml/release/dblp-2020-10-01.xml.gz
- Extract the zip in tour preferable location (Just remember the path you need to specify to the docker)
- Clone the project in your preferable location
- Build the docker
```
docker build -t xml_reducer .
```
- Run the docker
```
docker run  -v /location/of/your/xml/folder:/data/ --name xml_reducer --rm  -d  -t xml_reducer
```

Then choose the way you want to process your data

1) Local. 

- Run the process
```
docker exec xml_reducer ./xml_reducer --way local /data/your.xml
```

Wait for the response !!!

2) Local but run in parallel  ## BENCHMARK WINNER 
Consider though the time it takes to download the remote file locally and extract it

- Run the process
```
docker exec xml_reducer ./xml_reducer --way localp /data/your.xml
```

3) Remote. Do not even bother to download or extract your file.
The process is considereing the remote file as a stream an processing it line by line

- Clone the project in your preferable location
- Build the docker
```
docker build -t xml_reducer .
```
- Run the docker
```
docker run  -v /folder/for/plot/image:/data/ --name xml_reducer --rm -d  -t xml_reducer
```
- Run the process
```
docker exec xml_reducer ./xml_reducer --way remote https://dblp.org/xml/release/dblp-2020-10-01.xml.gz
```


## Plot
If you want to plot your processed data run the exec command with option --plot in the end
e.g.
```
docker exec xml_reducer ./xml_reducer --way localp /data/your.xml --plot
```
The image/chart will be stored at the mapped folder that you specified in the -v section of docker run command

Wait for the response !!!


** In the remote version there is a timeout of 15 secs for the streaming...in case server does not respond process may fail **


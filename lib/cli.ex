defmodule XmlReducer.CLI do
  @moduledoc """
    synopsis:
      Categorizing papers from dblp.org and count per year
    usage:
      $ dblp {options} uri
    options:
      --way local | localp | remote
      --plot
    """

  alias XmlReducer
  alias XmlReducer.Plot

  def main([help_opt]) when help_opt == "-h" or help_opt == "--help" do
    IO.puts(@moduledoc)
  end

  def main(args) do
    with {opts, cmd_and_args, errors} <- parse_args(args) do
      case errors do
        [] ->
          uri = [cmd_and_args]

          case opts[:way] do
            nil ->
              IO.puts("You must specify a way to run the process!!!")
              IO.puts(@moduledoc)

            "local" ->
              result = XmlReducer.local(uri)
              if opts[:plot], do: Plot.plot_data(result)
            "localp" ->
              result = XmlReducer.local_parallel(uri)
              if opts[:plot], do: Plot.plot_data(result)
            "remote" ->
              result = XmlReducer.remote(uri)
              if opts[:plot], do: Plot.plot_data(result)
          end
        _ ->
          IO.puts("Bad option:")
          IO.inspect(errors)
          IO.puts(@moduledoc)
      end

    else
      {:error, error} -> IO.puts(error)
      _ -> IO.puts("SYSTEM ERROR")
    end
  end

  defp parse_args(args) do
    {opts, cmd_and_args, errors} =
      args
      |> OptionParser.parse(strict:
        [way: :string, help: :boolean, plot: :boolean])

    with 1 <- length(cmd_and_args) do
      {opts, cmd_and_args, errors}
    else
      _ -> {:error, "You should only use one argument (uri) to run"}
    end

  end
end

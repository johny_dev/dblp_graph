defmodule XmlReducer do
  alias XmlReducer.HTTPStream

  @doc """
   default remote_url = "https://dblp.org/xml/release/dblp-2020-10-01.xml.gz"
  """
  def remote(remote_url \\ "https://dblp.org/xml/release/dblp-2020-10-01.xml.gz") do
    IO.puts("START PROCESSING.....")

    start_time = Time.utc_now()
    regex = Regex.compile!("^(<year>)(.*?)(<\/year>)")

    result = remote_url
    |> HTTPStream.get()
    |> StreamGzip.gunzip()
    |> HTTPStream.lines()
    |> Enum.reduce(%{}, fn line, result -> reducer(line, result, regex) end)
    |> Enum.to_list()
    |> Enum.sort(fn({year1, _}, {year2, _}) -> year1 <= year2 end )


    result
    |> Enum.each(fn {year, count} -> IO.puts("Year: #{year} -> #{count} publications") end)

    end_time = Time.utc_now()
    diff = Time.diff(end_time, start_time)

    IO.puts("FINISHED...Took #{diff} seconds")

    result
  end


  def local_parallel(file_uri) do
    IO.puts("START PROCESSING.....")

    start_time = Time.utc_now()
    regex = Regex.compile!("^(<year>)(.*?)(<\/year>)")

    result = file_uri
    |> File.stream!
    |> Flow.from_enumerable()
    |> Flow.partition()
    |> Flow.reduce(fn -> %{} end, fn line, result -> reducer(line, result, regex) end)
    |> Enum.to_list()
    |> Enum.sort(fn({year1, _}, {year2, _}) -> year1 <= year2 end )

    result
    |> Enum.each(fn {year, count} -> IO.puts("Year: #{year} -> #{count} publications") end)

    end_time = Time.utc_now()
    diff = Time.diff(end_time, start_time)

    IO.puts("FINISHED...Took #{diff} seconds")

    result
  end




  def local(file_uri) do
    IO.puts("START PROCESSING.....")

    start_time = Time.utc_now()
    regex = Regex.compile!("^(<year>)(.*?)(<\/year>)")

    result = file_uri
    |> File.stream!
    |> Enum.reduce(%{}, fn line, result -> reducer(line, result, regex) end)
    |> Enum.to_list()
    |> Enum.sort(fn({year1, _}, {year2, _}) -> year1 <= year2 end )

    result
    |> Enum.each(fn {year, count} -> IO.puts("Year: #{year} -> #{count} publications") end)

    end_time = Time.utc_now()
    diff = Time.diff(end_time, start_time)

    IO.puts("FINISHED...Took #{diff} seconds")

    result
  end

  defp reducer(line, result, regex) do

    with [_, _, year, _] <- process_line(line, regex),
      year <-  String.trim(year)
    do
      result = result[year]
      |> process(year, result)

      result
    else
      _ -> result
    end
  end

  defp process(nil, year, result), do: Map.put(result, year, 1)
  defp process(_, year, result), do: Map.put(result, year, result[year] + 1)

  defp process_line(line, regex) do
    Regex.run(regex, line)
  end

end

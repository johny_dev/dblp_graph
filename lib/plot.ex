defmodule XmlReducer.Plot do
  import Gnuplot

  def plot_data(data) do
    chart = [
      [:set, :term, :png, :size, '2048,1025'],
      [:set, :output, Path.join("/data", "barchart.PNG")],
      [:set, :boxwidth, 0.5],
      [:set, :xlabel, "years"],
      [:set, :xtics, :rotate, :by, 60, :right],
      [:set, :ylabel, "publications"],
      ~w(set style fill solid)a,
      [:plot, "-", :using, '1:3:xtic(2)', :with, :boxes,:"," ,"-", :using, '0:1:1', :with, :labels]
    ]

    plot(chart, [map_data(data)])
  end

  defp map_data(data) do
    {_, dataset} = data
    |> Enum.reduce({0, []}, &transform_pair/2)

    dataset
  end

  defp transform_pair({year, count}, {index, result}) do
    result = [[index, year, count] | result]
    {index+1, result}
  end
end

defmodule XmlReducerTest do
  use ExUnit.Case
  doctest XmlReducer

  test "processing_xml" do
    regex = Regex.compile!("^(<year>)(.*?)(<\/year>)")

    File.stream!("lib/test.xml")
    |> Enum.reduce(%{}, fn line, result -> reducer(line, result, regex) end)
    |> Enum.to_list()
    |> Enum.sort(fn({year1, _}, {year2, _}) -> year1 <= year2 end )
    |> Enum.each(fn {year, count} -> IO.puts("Year: #{year} -> #{count}") end)
  end
end
